angular.module('app.profileController', [])

  .controller('profileCtrl', function ($scope, $http, $rootScope, $state, profileService) {

    var getProfile = function () {
      $http.get($rootScope.urlParkapp + 'profile')
        .success(
          function (data) {
            $scope.profile =  data.profile;
            profileService.perfil = $scope.profile;
          });
    };

    getProfile();

    $rootScope.$on('profileCtrl:getProfile', function() {
      getProfile();
    });

    $scope.sairPerfil = function () {
      $scope.$emit('logoutCtrl:setLogout');
    }
  })

  .controller('changeInfoCtrl', function ($scope, $http, $rootScope, $state, profileService) {

    $scope.profile  = profileService.perfil;

    $scope.salvar = function () {

      Object.toparams = function ObjecttoParams(obj) {
        var p = [];
        for (var key in obj) {
          p.push('profile['+ key + ']=' + encodeURIComponent(obj[key]));
        }
        return p.join('&');
      };

      $http.put($rootScope.urlParkapp + 'profile?' + Object.toparams( $scope.profile ))
        .success(
          function (data) {
            $scope.$emit('profileCtrl:getProfile');
            $state.go('profile');
          });

    }

  })

  .controller('changePasswordCtrl', function ($scope, $http, $rootScope, $state, profileService) {

    $scope.profile  = profileService.perfil;

    $scope.salvar = function () {

      // Object.toparams = function ObjecttoParams(obj) {
      //   var p = [];
      //   for (var key in obj) {
      //     p.push('profile['+ key + ']=' + encodeURIComponent(obj[key]));
      //   }
      //   return p.join('&');
      // };
      //
      // $http.put($rootScope.urlParkapp + 'profile?' + Object.toparams( $scope.profile ))
      //   .success(
      //     function (data) {
      //       $scope.$emit('perfilCtrl:getProfile');
      //       $state.go('profile');
      //     });

    }

  })

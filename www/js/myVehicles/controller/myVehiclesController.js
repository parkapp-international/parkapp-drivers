/**
 * Created by Márcio on 05/07/2016.
 */

var app = angular.module('app.myVehiclesController', []);

app.controller('myVehiclesCtrl', function ($scope, $http, $state, $filter, $ionicLoading, $ionicPopup, $rootScope, messageService, vehiclesService) {

  var getMyVehicles = function () {

    $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});

    $http.get($rootScope.urlParkapp + 'account/user_vehicles')
      .success(function (data) {
        $scope.vehicles = data.user_vehicles;
      })
      .error(function (result) {
        messageService.showErrors(result.errors);
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };

  var removeVehicle = function (vehicle) {
    $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});

    $http.delete($rootScope.urlParkapp + 'account/user_vehicles/' + vehicle.id)
      .success(function (data) {
        messageService.showSuccess($filter('translate')('SUCCESS_REMOVE_CREDIT_CARD'));
        getMyVehicles();
      })
      .error(function (result) {
        messageService.showErrors(result.errors);
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };

  getMyVehicles();

  $scope.vehicles = [];

  var addedVehicles = function() {
      getMyVehicles();
      $state.go('driverHome');
  }

  $scope.addVehicle = function () {
    vehiclesService.registerVehiclePopup($scope, addedVehicles);
  };

  $scope.doFocus = function (elementId) {

    if(angular.isDefined(window.cordova)) {
      var elem = angular.element(document.querySelector(elementId));
      var isAndroid = ionic.Platform.isAndroid();
      console.log(ionic.Platform);
      if(isAndroid) {
          cordova.plugins.Focus.focus(elem[0]);
      }else {
          elem[0].focus();
      }
    }
  };

  $scope.getVehicleInfo = function() {
    elem = angular.element(document.querySelector('#modelId'));
    elem[0].focus();
  };

  $scope.manageLettersPlateField = function() {
    if ($scope.vehicle.plateLetra.length == 3) {
      var elem = angular.element(document.querySelector('#plateLetraId'));
      elem[0].blur();
      var nextElem = angular.element(document.querySelector('#plateNumeroId'));
      nextElem[0].focus();
    }
  };

  $scope.manageNumbersPlateField = function() {
    if ($scope.vehicle.plateNumero.length == 4) {
      var elem = angular.element(document.querySelector('#plateNumeroId'));
      elem[0].blur();
      $scope.getVehicleInfo();
    } else if ($scope.vehicle.plateNumero.length == 0) {
      var elem = angular.element(document.querySelector('#plateLetraId'));
      elem[0].focus();
    }
  };

  $scope.onNumberClicked = function() {
    $scope.vehicle.plateNumero = '';
    $scope.vehicle.plateLetra = '';
    $scope.vehicle.model = '';
    $scope.vehicle.color = '';
    var elem = angular.element(document.querySelector('#plateLetraId'));
    elem[0].focus();
  };

  $scope.onPlateFocus = function() {
    if (angular.isUndefined($scope.vehicle.plateLetra) || $scope.vehicle.plateLetra.length == 0) {
      $scope.vehicle.plateNumero = '';
      $scope.vehicle.plateLetra = '';
      var elem = angular.element(document.querySelector('#plateLetraId'));
      elem[0].focus();
    }
  };

  $scope.changeVehicle = function (vehicle) {

    var changeVehiclePopup = $ionicPopup.show({
      cssClass: 'popup popup-vertical-buttons',
      title: '' + $filter('translate')('DO_YOU_REALLY_WANT_TO_CHANGE_VEHICLE'),
      template: '<div class="row row-center">'+
        '<div class="card-info">'+
        '<p class="text-center">' + $filter('translate')('DONT_WORRY_YOU_CAN_CHANGE_ANYTIME') + '</p>'+
        '</div>'+
        '</div>',
      buttons: [
        {
          text: '<b>' + $filter('translate')('YES_CHANGE_SELECTED_VEHICLE') + '</b>',
          type: 'button-positive button-block',
          onTap: function (e) {
            return 0;
          }
        },
        {
          text: '<b>' + $filter('translate')('CANCEL') + '</b>',
          type: 'button-stable button-block',
          onTap: function (e) {
            return 1;
          }
        }
      ]
    });
    changeVehiclePopup.then(function (res) {
      if (res === 0)
        console.log(vehicle);
        //TODO implementar mudança de veículo
      else
        changeVehiclePopup.close();
    });
  };

  $scope.removeVehicle = function (vehicle) {
    messageService.showConfirmAndDo($filter('translate')('REMOVE_VEHICLE'), $filter('translate')('DO_YOU_REALLY_WANT_TO_REMOVE_VEHICLE'), removeVehicle, vehicle);
  };
});

/**
 * Created by Márcio on 05/07/2016.
 */

var app = angular.module('app.myAccountController', []);

app.controller('myAccountCtrl', function ($scope, $http, $state, $filter, $ionicLoading, $ionicPopup, $rootScope, messageService) {

  var getMyCreditCards = function () {

    $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});

    $http.get($rootScope.urlParkapp + 'account/credit_cards')
      .success(function (data) {
        $scope.cards = data.credit_cards;
      })
      .error(function (result) {
        messageService.showErrors(result.errors);
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };

  var removeCreditCard = function (card) {
    $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});

    $http.delete($rootScope.urlParkapp + 'account/credit_cards/' + card.id)
      .success(function (data) {
        messageService.showSuccess($filter('translate')('SUCCESS_REMOVE_CREDIT_CARD'));
        getMyCreditCards();
      })
      .error(function (result) {
        messageService.showErrors(result.errors);
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  }

  getMyCreditCards();

  $scope.cards = [];

  $scope.addCreditCard = function () {
    $state.go("addCreditCard");
  };

  $scope.removeCreditCard = function (card) {

    messageService.showConfirmAndDo($filter('translate')('REMOVE_CREDIT_CARD'), $filter('translate')('DO_YOU_REALLY_WANT_TO_REMOVE_CREDIT_CARD'), removeCreditCard, card);

  };

});

app.controller('addCreditCardCtrl', function ($scope, $http, $state, $ionicLoading, constants, messageService, $filter, $rootScope) {

  $scope.card = {};

  $scope.handleCreditNumberFocus = function() {
      if($scope.card.number.length === 16) {
          var elem = angular.element(document.querySelector('#codSeguranca'));
          var isAndroid = ionic.Platform.isAndroid();
          if(isAndroid) {
              cordova.plugins.Focus.focus(elem[0]);
          }else {
              elem[0].focus();
          }
      }
  };

  var getYears = function () {

    var currentYear = {number: new Date().getFullYear()};
    var years = [];

    years.push(currentYear);

    for (var i = 1; i < 16; i++) {
      years.push({number: currentYear.number + i});
    }

    return years;
  };

  var getHash = function() {
    var cc = new Moip.CreditCard({
      number  : $scope.card.number,
      cvc     : $scope.card.cvv2,
      expMonth: $scope.card.expire_month,
      expYear : $scope.card.expire_year,
      pubKey  : constants.MOIP_PUBLIC_KEY_PRODUCTION
    });
    return cc.hash();
  };

  $scope.months = [{number: 01}, {number: 02}, {number: 03}, {number: 04}, {number: 05}, {number: 06}, {number: 07}, {number: 08}, {number: 09}, {number: 10}, {number: 11}, {number: 12}];
  $scope.years = getYears();

  $scope.card.expire_month = 1;
  $scope.card.expire_year = new Date().getFullYear();

  $scope.addNewCard = function () {

    $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});

    $scope.card.credit_card_hash = getHash();

    var card_information = {card_information: $scope.card};

    $http.post($rootScope.urlParkapp + 'account/credit_cards', card_information)
      .success(function (data) {
        messageService.showSuccess($filter('translate')('SUCCESS_CREATE_CREDIT_CARD'));
        $state.go("myAccount");
      })
      .error(function (result) {
        messageService.showErrors(result.errors);
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };
});

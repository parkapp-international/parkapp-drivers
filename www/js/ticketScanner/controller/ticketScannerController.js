/**
 * Created by Márcio on 15/07/2016.
 */

var app = angular.module('app.ticketScannerController', []);

app.controller('ticketScannerCtrl', function ($scope, $cordovaBarcodeScanner, $http, $state, $filter, $ionicLoading, $ionicPopup, $rootScope, messageService) {

  $scope.barcode;

  $scope.scanBarcode = function () {
    $cordovaBarcodeScanner.scan().then(function (imageData) {
      $scope.barcode = imageData.text;
      if (!$scope.$$phase) {
        $scope.$apply();
      }

      var elem = angular.element(document.querySelector('#barcode'));
      elem.val(imageData.text);
    }, function (error) {
      messageService.showError("Occoreu um erro: " + error);
    });
  };

  $scope.searchTicket = function () {

    if (angular.isUndefined($scope.barcode) || !$scope.barcode) {
      messageService.showError("Você deve informar o código a ser consultado.");
      return;
    }

    //TODO implementar chamada do serviço quando o mesmo for disponibilizado
    getMyCreditCards();

  };

  var getMyCreditCards = function () {

    $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});

    $http.get($rootScope.urlParkapp + 'account/credit_cards')
      .success(function (data) {

        if (!data.credit_cards || data.credit_cards.length == 0) {
          var message = "Favor cadastrar um cartão de crédito para continuar.";
          var tittle = "Nenhum cartão cadastrado";
          messageService.showAlert(message, tittle);
        } else {
          $scope.myCreditCards = data.credit_cards;
          openPaymentPopup();
        }
      })
      .error(function (result) {
        messageService.showErrors(result.errors);
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };

  var openPaymentPopup = function () {

    var paymentPopup = $ionicPopup.show({
      template: '',
      cssClass: 'popup-vertical-buttons',
      title: 'Shopping Beiramar',
      templateUrl: 'templates/driver/paymentPopup.html',
      scope: $scope,
      buttons: [
        {
          text: '<b>' + $filter('translate')('CONFIRM_PAYMENT') + '</b>',
          type: 'button-balanced button-block',
          onTap: function (e) {
            return 0;
          }
        },
        {
          text: '<b>' + $filter('translate')('CANCEL') + '</b>',
          type: 'button-stable button-block',
          onTap: function (e) {
            return 1;
          }
        }
      ]
    });

    paymentPopup.then(function (res) {
      if (res === 0)
      //TODO implementar pagamento de ticket
        $ionicPopup.hide();
      else
        $ionicPopup.hide();
    });
  };

});

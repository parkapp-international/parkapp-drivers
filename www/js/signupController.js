angular.module('app.signupController', [])

  .controller('signupCtrl', function ($scope, $http, $rootScope, $state, $ionicLoading, $filter, messageService, $rootScope, constants) {

    $scope.$watch(
      function () {
        return localStorage.getItem('user_email') + localStorage.getItem('user_pass');
      }
      , function () {
        updateFields();
      }
    );

    var updateFields = function () {
      var emailLS = localStorage.getItem('user_email');
      var passLS = localStorage.getItem('user_pass');
      var name = localStorage.getItem('user_name');

      var email = emailLS === "undefined" ? "" : emailLS;
      var pass = passLS === "undefined" ? "" : passLS;

      $scope.profile = {
        default_context: constants.CONTEXT_DRIVER,
        email: email,
        password: pass,
        name: name
      };
    };

    // login: localStorage.getItem('user_token')

    $scope.cadastrar = function () {

    //   console.log("aa", $scope.profile);

      $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});

      Object.toparams = function ObjecttoParams(obj) {
        var p = [];
        for (var key in obj) {
          p.push('profile[' + key + ']=' + encodeURIComponent(obj[key]));
        }
        return p.join('&');
      };

      $http.post($rootScope.urlParkapp + 'profile?' + Object.toparams($scope.profile))
        .success(
          function (data) {

            messageService.showSuccess($filter('translate')('SIGNUP_SUCCESS'));
            localStorage.setItem('user_token', data.profile.user_token);
            localStorage.setItem('user_email', data.profile.email);
            localStorage.setItem("profile", angular.toJson($scope.profile));

            $rootScope.profile = $scope.profile;
            $rootScope.isAuthenticated = true;

            $http({
              url: "https://www.rdstation.com.br/api/1.3/conversions",
              method: "PUT",
              headers: {
               'Content-Type': "application/json"
              },
              data : {
                "token_rdstation": "61538b3fedfa8ea4b99f99e2aac73d8e", //Obrigatório - Token Publico de autenticação
                "identificador": "Cadastro user",
                "email": data.profile.email,
                "Nome" : data.profile.name,
                "Telefone" : data.profile.cellphone
              }
            })
            .success(function (data) {
                console.log('bão', data);
            //   messageService.showSuccess('Mensalista cadastrado com sucesso!');
            })
            .error(function (result) {
                console.log('zicou', result);
            //   messageService.showError('Não foi possível efetuar o cadastro.');
            });

            $state.go('myVehicles');
          })
        .error(function (result) {
          messageService.showErrors(result.errors);
        })
        .finally(function () {
          $ionicLoading.hide();
        });
    }
  });

angular.module('app.supportController', [])


  .controller('supportCtrl', function ($scope, $http, $rootScope, tawkToService, messageService) {

    $scope.vm = {};

    $scope.vm.visitor = {};
    $scope.vm.loaded = false;

    $scope.$on('TAWKTO:onLoad', function () {
      console.log("oie");
      $scope.$apply(setVisitor);
      $scope.vm.loaded = true;
    });

    //YOUR TAWK.TO ID GOES HERE
    $scope.vm.id = '574e2dde95a174305a41a75e';

    $scope.vm.toggle = tawkToService.toggle;
    $scope.vm.toggleVisibility = tawkToService.toggleVisibility;

    function setVisitor() {
      $scope.vm.visitor = tawkToService.setVisitor('ngTawkTo Demo User', 'demo@demo.com');
    }

    if (!$scope.vm.id) {
      messageService.showError("Erro ao conectar ao chat");
    }

  })



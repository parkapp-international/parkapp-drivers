// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'


app = angular.module('app', ['ionic', 'ui.utils.masks', 'pascalprecht.translate', 'ngCordova', 'idf.br-filters', 'uiGmapgoogle-maps',

  'app.loginController',
  'app.signupController',
  'app.profileController',
  'app.supportController',

  'app.driverHomeController',
  'app.myAccountController',
  'app.myVehiclesController',
  'app.activeSessionController',
  'app.ticketScannerController',

  'app.messageService',
  'app.notificationService',
  'app.profileService',
  'app.vehiclesService',
  'app.driverHomeService',

  'app.yesNo',

  'app.routes']);

app.constant("constants", {
  "CONTEXT_DRIVER": "motorista",
  "CONTEXT_PARK": "estacionamento",
  "CONTEXT_ADMIN": "manager",
  "MAPS_API_KEY": "AIzaSyAV63Y0FlPuRtqmFBunIehlAlFTxMjhmKw",
  "MOIP_PUBLIC_KEY_PRODUCTION": "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgJH+6l5k9endb+yaL465vWogxOVygPRqmo0UHlyuHE8VGCVBkz4B+arhd1I3UUb" +
    "/Vuo9RltciEpeO/Hi/fgn9VZaeDNb4TK+vkzjrBIrFlHKKvAxl+5mf2yGhDs8HjPJW9VnHo9eqMFLIvVOHj7gE7Eu6CF5UzO0IoTxXtI8eS0ONnCyH1Nd8SYzgRUlD3Y4kmrm44uTVjegRtCWhFX38/ti6j/9T0H7Lh" +
    "aAV8cFOLHaB6Pg7tvALWrpnwdu3ooRYdnrRvshzRW5bL1pjBlZ2sX4V8CtWWtSGDjZQX0d4MxZh7Em3mrOHqMZF9R9g/mvVgjFMGkbQzHDNksjHkV97QIDAQAB-----END PUBLIC KEY-----",
  "MOIP_PUBLIC_KEY_DEVELOPMENT": "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAi2pzaN+JlFugp/bM9LsdkxVSMFGEWrLvaf9bQY9bC/zzVX30YR4lGiKve0LWEY" +
    "KlcQmLBIx0iWiKwkAIs+LzENLWuMI+Hu46cszPk4fs2yULYADkzYPIcLPKvvPifX3IgHaWYhzKihC74h5pGuRe4CoNVekolgfL1uO6/+mv9eVSFkJACUWKKKlkzT9Oro6yxE2yMhJYKY/2ZXM5ae5Oc9ONmdEP5e09F" +
    "LgNB9UmstA+JOBymkmvZsn56ej5FFoA6zBcFyBONvwvhK7gCgIH7ffX/TcAniQKHdr3ZXthAUh68yWwVdNkKusLRSHiVyRvO1KlUwhsWZCK3I2GsJkZnwIDAQAB-----END PUBLIC KEY-----"
});

app.run(function ($ionicPlatform, $http, $state, $rootScope, $ionicHistory, $cordovaVibration, $cordovaNativeAudio, $ionicSideMenuDelegate, $timeout, $ionicModal, $ionicPopup, $cordovaNetwork) {
  $ionicPlatform.ready(function () {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    $rootScope.isOnline = $cordovaNetwork.isOnline();

    $ionicSideMenuDelegate.canDragContent(false);

    $ionicPlatform.registerBackButtonAction(function (event) {
      if ($state.current.name !== "driverHome") {
        navigator.app.backHistory();
      }
    }, 100);

    window.addEventListener('native.keyboardshow', function () {
      document.querySelector('div.tabs').style.display = 'none';
      angular.element(document.querySelector('ion-content.has-tabs')).css('bottom', 0);
    });

    window.addEventListener('native.keyboardhide', function () {
      var tabs = document.querySelectorAll('div.tabs');
      angular.element(tabs[0]).css('display', '');
    });

    $rootScope.urlParkapp = "https://appipark.com/api/";
    // $rootScope.urlParkapp = 'https://ipark-dev.herokuapp.com/api/';
    $rootScope.isAuthenticated = false;

  });
});

app.config(function ($httpProvider, $translateProvider, $ionicConfigProvider, uiGmapGoogleMapApiProvider) {

  $ionicConfigProvider.tabs.position('bottom'); // other values: top

  $httpProvider.interceptors.push('authInterceptor');
  $httpProvider.defaults.useXDomain = true;
  $httpProvider.defaults.cache = false;
  delete $httpProvider.defaults.headers.common['X-Requested-With'];

  uiGmapGoogleMapApiProvider.configure({
    key: "AIzaSyAV63Y0FlPuRtqmFBunIehlAlFTxMjhmKw",
    v: '3.17',
    libraries: '',
    language: 'pt',
    sensor: 'false'
  });

  $translateProvider
    .useStaticFilesLoader({
      prefix: 'conf/locales/',
      suffix: '.json'
    })
    .registerAvailableLanguageKeys(['pt-BR', 'en-US'], {
      'en': 'en', 'en_GB': 'en-GB', 'en_US': 'en-US',
      'pt': 'pt', 'pt_BR': 'pt-BR', 'pt_PT': 'pt-PT'
    })
    .preferredLanguage('pt-BR')
    .fallbackLanguage('pt-BR')
    .determinePreferredLanguage()
    .useSanitizeValueStrategy('escapeParameters');

});

app.factory('authInterceptor', function ($rootScope, $q) {

  return {

    request: function (config) {

      $rootScope.$broadcast('loading:show');

      if (localStorage.getItem('user_token') && localStorage.getItem('user_email')) {

        var url = config.url;
        if (url.indexOf($rootScope.urlParkapp) > -1) {
          var urlNova = insertParam('user_email', localStorage.getItem('user_email'), url);
          urlNova = insertParam('user_token', localStorage.getItem('user_token'), urlNova);

          config.url = urlNova;

        }

      }
      return config;

    },
    response: function (response) {

      $rootScope.$broadcast('loading:hide');

      return response || $q.when(response);
    },

    responseError: function (rejection) {

      $rootScope.$broadcast('loading:hide');

      if (rejection.status === 422) {

        $rootScope.$broadcast('errorPopup:show', rejection.data.errors);

      }
      return $q.reject(rejection);
    }
  };
});

function insertParam(key, value, url) {
  key = encodeURI(key);
  value = encodeURIComponent(value);

  if ((url.indexOf('&') > -1) || (url.indexOf('?') > -1)) {
    var kvp = url.substr(0).split('&');

    var i = kvp.length;
    var x;
    while (i--) {
      x = kvp[i].split('=');

      if (x[0] == key) {
        x[1] = value;
        kvp[i] = x.join('=');
        break;
      }
    }

    if (i < 0) {
      kvp[kvp.length] = [key, value].join('=');
    }

    //this will reload the page, it's likely better to store this until finished
    return kvp.join('&');
  } else {
    return url + '?' + key + '=' + value;
  }
};

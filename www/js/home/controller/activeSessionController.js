/**
 * Created by Márcio on 05/07/2016.
 */

var app = angular.module('app.activeSessionController', []);

app.controller('activeSessionCtrl', function ($scope, $state, $ionicLoading, driverHomeService, $ionicPopup, $filter, $http, $rootScope, messageService, $interval) {

  $scope.markers = [];
  $scope.parkTicket = {};
  $scope.deliveryRequest = false;
  $scope.myCreditCards = [];
  $scope.selectedCard = {};

  $scope.map = {
    center: {latitude: -27.600988, longitude: -48.523316},
    zoom: 16
  }

  $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
    viewData.enableBack = false;
    $scope.parkTicket = driverHomeService.parkTicket;
    initMap();
  });

  var getTickets = function() {

    $http.get($rootScope.urlParkapp + '/account/my_tickets')
      .success(function (data) {
        if (!data.park_tickets || data.park_tickets.length == 0) {
          $state.go('driverHome');
        }
      })
      .error(function (result) {
        messageService.showErrors(result.errors);
      });
  };

  $interval(function () {
    getTickets();
  }, 5000);

  var initMap = function () {
    $scope.map = {
      center: {
        latitude: $scope.parkTicket.parking_lot.latitude,
        longitude: $scope.parkTicket.parking_lot.longitude
      },
      zoom: 16
    };
    $scope.markers.push({
      id: $scope.parkTicket.id,
      latitude: $scope.parkTicket.parking_lot.latitude,
      longitude: $scope.parkTicket.parking_lot.longitude,
    })
  };

  var openSelectCardPopup = function () {
    var selectCardPopup = $ionicPopup.show({
      cssClass: 'popup-vertical-buttons registerVehicle-popup',
      title: $filter('translate')('SELECT_CREDIT_CARD'),
      template: '<select ng-model="selectedCard.id" class="settings-select" ng-options="card.id as card.description for card in myCreditCards">' +
      '</select>',
      scope: $scope,
      buttons: [
        {
          text: '<b>' + $filter('translate')('CONFIRM_PAYMENT') + '</b>',
          type: 'button-balanced button-block',
          onTap: function (e) {
            return 0;
          }
        },
        {
          text: '<b>' + $filter('translate')('CANCEL') + '</b>',
          type: 'button-stable button-block',
          onTap: function (e) {
            return 1;
          }
        }
      ]
    });

    selectCardPopup.then(function (res) {
      if (res === 0)
        payTicket();
    //   else
    //     $ionicPopup.hide();
    });
  };

  var payTicket = function () {

    $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});

    var payment = {
      payment: {
        value: $scope.parkTicket.session_price.current_price,
        credit_card_id: $scope.selectedCard.id
      }
    };


    var profile = angular.fromJson(localStorage.getItem("profile"));

    $http({
      url: "https://www.rdstation.com.br/api/1.3/conversions",
      method: "PUT",
      headers: {
       'Content-Type': "application/json"
      },
      data : {
        "token_rdstation": "61538b3fedfa8ea4b99f99e2aac73d8e", //Obrigatório - Token Publico de autenticação
        "identificador": "Motorista pagou via app",
        "email": profile.email
      }
    })
    .success(function (data) {
        console.log('bão', data);
    //   messageService.showSuccess('Mensalista cadastrado com sucesso!');
    })
    .error(function (result) {
        console.log('zicou', result);
    //   messageService.showError('Não foi possível efetuar o cadastro.');
    });

    $http.post($rootScope.urlParkapp + 'account/my_tickets/' + $scope.parkTicket.id + '/pay', payment)
      .success(function (data) {

        console.log("10", data);

        var alertPopup = $ionicPopup.alert({
            template: 'Pagamento efetuado com sucesso!'
        });

        alertPopup.then(function(res) {
            getTickets();
            $state.go('driverHome');
        });
      })
      .error(function (result) {
        messageService.showErrors(result.errors);
      })
      .finally(function () {
        $ionicLoading.hide();
      });

  };

  var getMyCreditCards = function () {

    $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});

    $http.get($rootScope.urlParkapp + 'account/credit_cards')
      .success(function (data) {

        if (!data.credit_cards || data.credit_cards.length == 0) {
          var message = "Favor cadastrar um cartão de crédito para continuar.";
          var tittle = "Nenhum cartão cadastrado";
          messageService.showAlert(message, tittle);
        } else {
          $scope.myCreditCards = data.credit_cards;
          openSelectCardPopup();
        }
      })
      .error(function (result) {
        messageService.showErrors(result.errors);
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };

  $scope.requestDelivery = function () {

    $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});

    $http.put($rootScope.urlParkapp + 'account/my_tickets/' + $scope.parkTicket.id + '/fetch')
      .success(function (data) {

        $scope.deliveryRequest = true;

        var title = "Parabéns! Seu carro foi solicitado com sucesso.";
        var message = "Por favor esteja pronto para retirar o seu veículo. Caso você não esteja presente no momento da entrega, seu carro voltara ao final da fila."

        messageService.showAlert(message, title);

      })
      .error(function (result) {
        messageService.showErrors(result.errors);
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };

  $scope.showMyCards = function () {
    $state.go('myAccount');
  };

  $scope.payMyParking = function () {
    if ($scope.myCreditCards.length > 0) {
      openSelectCardPopup;
    } else {
      getMyCreditCards();
    }
  };
});

/**
 * Created by Márcio on 05/07/2016.
 */

var app = angular.module('app.driverHomeController', []);

app.controller('driverHomeCtrl', function ($scope, $cordovaGeolocation, $state, $interval, $ionicHistory, $filter, $ionicPopup, $rootScope, $ionicLoading, $http,
                                           $rootScope, messageService, vehiclesService, driverHomeService) {

  $scope.markers = [];
  $scope.parking_lots = [];

  $scope.map = {
    center: {latitude: -27.600988, longitude: -48.523316},
    zoom: 16
  }

  var mountQueryParamsPark_ticket = function (lat, long) {
    var query = '';

    query += 'latitude=' + encodeURIComponent(lat);
    query += '&longitude=' + encodeURIComponent(long);

    return query;
  };

  var getAmount = function (priceTabs) {

    var amount = "  --";

    if (priceTabs[0] != null) {
      if (priceTabs[0].parking_prices[0] != null) {
        amount = $filter('finance')(priceTabs[0].parking_prices[0].value);
      }
    }
    return amount;
  };

  var createMarkers = function () {

    angular.forEach($scope.parking_lots, function (parkingLot) {
      $scope.markers.push({
        id: parkingLot.id,
        latitude: parkingLot.latitude,
        longitude: parkingLot.longitude,
        title: parkingLot.name,
        address: parkingLot.address,
        price_tabs: parkingLot.price_tabs,
        spot_number: parkingLot.spot_number,
        icon: 'img/mapMarker.png',
        options: {
          labelClass: 'markerLabel',
          labelAnchor: '15 40',
          labelContent: getAmount(parkingLot.price_tabs)
        }
      });
    });
  };

  var getParkingLots = function (lat, long) {

    $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});

    $http.get($rootScope.urlParkapp + 'parking_lots/search?' + mountQueryParamsPark_ticket(lat, long))
      .success(function (data) {
        $scope.parking_lots = data.parking_lots;
        $rootScope.parking_lots = data.parking_lots;
        createMarkers();
      })
      .error(function (result) {
        messageService.showErrors(result.errors);
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };

  var initMap = function () {

    $scope.mapInitialized = true;

    // get position of user and then set the center of the map to that position
    $cordovaGeolocation.getCurrentPosition()
      .then(function (position) {

        var lat = position.coords.latitude;
        var long = position.coords.longitude;

        $scope.map = {
          center: {latitude: lat, longitude: long},
          zoom: 16,
          bounds: {},
          markersEvents: {
            click: function (marker, eventName, model, arguments) {
              openDetails(marker, eventName, model, arguments);
            },
            mousedown: function (marker, eventName, model, arguments) {
              openDetails(marker, eventName, model, arguments);
            }
          }
        };

        if (angular.isUndefined($rootScope.parking_lots) || $rootScope.parking_lots.length == 0)
          getParkingLots(lat, long);
        else
          createMarkers();
      }, function (err) {
        messageService.showErrors(err);
      });
  };

  var getTickets = function () {

    $http.get($rootScope.urlParkapp + '/account/my_tickets')
      .success(function (data) {

          console.log(data);

        if (data.park_tickets && data.park_tickets.length === 1) {
          driverHomeService.parkTicket = data.park_tickets[0];
          if($state.current.name === 'driverHome')
            $state.go('activeSession');
        } else {
          if(!$scope.mapInitialized)
            initMap();
        }
      });
  };

  $interval(function () {
    getTickets();
  }, 50000);

  $scope.$on('$ionicView.beforeEnter', function (event, viewData) {

      console.log('entrou');

    viewData.enableBack = false;
    getTickets();
  });

  var openDetails = function (marker, eventName, model, arguments) {

    $scope.selectedParkingLot = model;
    $scope.selectedParkingLot.map = {
      center: {
        latitude: model.latitude,
        longitude: model.longitude
      },
      zoom: 16,
      bounds: {}
    };

    var changeVehiclePopup = $ionicPopup.show({
      template: '',
      cssClass: 'popup-vertical-buttons',
      title: model.title,
      templateUrl: 'templates/home/parkingLotDatailPopup.html',
      scope: $scope,
      buttons: [
        {
          text: '<b>' + $filter('translate')('GO_BACK') + '</b>',
          type: 'button-positive button-block',
          onTap: function (e) {
            return 0;
          }
        }
      ]
    });
    changeVehiclePopup.then(function (res) {
      $ionicPopup.hide();
    });

  };

  $scope.doFocus = function (elementId) {

    if(angular.isDefined(window.cordova)) {
      var elem = angular.element(document.querySelector(elementId));
      var isAndroid = ionic.Platform.isAndroid();
      if(isAndroid) {
          cordova.plugins.Focus.focus(elem[0]);
      }else {
          elem[0].focus();
      }
    }
  };

  $scope.getVehicleInfo = function() {
    elem = angular.element(document.querySelector('#modelId'));
    elem[0].focus();
  };

  $scope.manageLettersPlateField = function() {
    if ($scope.vehicle.plateLetra.length == 3) {
      var elem = angular.element(document.querySelector('#plateLetraId'));
      elem[0].blur();
      var nextElem = angular.element(document.querySelector('#plateNumeroId'));
      nextElem[0].focus();
    }
  };

  $scope.manageNumbersPlateField = function() {
    if ($scope.vehicle.plateNumero.length == 4) {
      var elem = angular.element(document.querySelector('#plateNumeroId'));
      elem[0].blur();
      $scope.getVehicleInfo();
    } else if ($scope.vehicle.plateNumero.length == 0) {
      var elem = angular.element(document.querySelector('#plateLetraId'));
      elem[0].focus();
    }
  };

  $scope.onNumberClicked = function() {
    $scope.vehicle.plateNumero = '';
    $scope.vehicle.plateLetra = '';
    $scope.vehicle.model = '';
    $scope.vehicle.color = '';
    var elem = angular.element(document.querySelector('#plateLetraId'));
    elem[0].focus();
  };

  $scope.onPlateFocus = function() {
    if (angular.isUndefined($scope.vehicle.plateLetra) || $scope.vehicle.plateLetra.length == 0) {
      $scope.vehicle.plateNumero = '';
      $scope.vehicle.plateLetra = '';
      var elem = angular.element(document.querySelector('#plateLetraId'));
      elem[0].focus();
    }
  };

  $scope.registerVehicle = function (scope) {
    vehiclesService.registerVehiclePopup(scope, function(){});
  };

  if ($rootScope.profile.vehicles && $rootScope.profile.vehicles.length == 0) {
    $scope.registerVehicle($scope);
  };

});

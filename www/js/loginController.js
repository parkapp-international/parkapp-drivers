angular.module('app.loginController', [])

  .controller('loginCtrl', function ($scope, $http, $rootScope, $state, $ionicLoading, messageService, $ionicPopup, constants) {

    $rootScope.urlParkapp = 'https://appipark.com/api/';
    $rootScope.$state = $state;

    $scope.user = {};

    /*$scope.user = {
     email: 'estac@teste.com',
     password: '123456'
     };*/
    if (localStorage.getItem('user_token') && localStorage.getItem('user_email')) {
      $rootScope.isAuthenticated = true;
      $rootScope.profile = angular.fromJson(localStorage.getItem("profile"));
      if ($rootScope.parking_lot = $rootScope.profile.default_parking_lot !== null) {

        console.log($rootScope.parking_lot);
        $rootScope.isDriver = angular.equals($rootScope.profile.default_context, constants.CONTEXT_DRIVER);
        $state.go('driverHome');
      }
    }

    var openDriverContext = function (data) {
      $state.go('driverHome');
    };

    $scope.signup = function () {
      localStorage.setItem('user_email', '');
      localStorage.setItem('user_pass', '');
      localStorage.setItem('user_name', '');
      $scope.user.email = '';
      $scope.user.password = '';
      $scope.user.name = '';
      $state.go('signup');
    };

    $scope.login = function () {

      var loginPopup = $ionicPopup.show({
        template: '',
        cssClass: 'popup-vertical-buttons',
        title: '',
        templateUrl: 'templates/loginPopup.html',
        scope: $scope,
        buttons: [
          {
            text: '<b>Entrar</b>',
            type: 'button-positive button-block',
            onTap: function (e) {
              return 0;
            }
          },
          {
            text: '<b>Esqueci minha senha</b>',
            type: 'button-link-disguised',
            onTap: function (e) {
              return 1;
            }
          }
        ]
      });

      var performLogin = function () {
        emailFieldId = angular.element(document.querySelector('#emailId'))[0].id;

        if (emailFieldId === document.activeElement.id) {
          passField = angular.element(document.querySelector('#password'))[0];
          passField.focus();
        } else {
          var urllogin = encodeURI('user[email]=' + $scope.user.email + '&user[password]=' + $scope.user.password);
          $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});
          $http.post($rootScope.urlParkapp + 'users/sign_in.json?' + urllogin)
            .success(function (data) {
              $rootScope.isAuthenticated = true;
              localStorage.setItem('user_token', data.profile.user_token);
              localStorage.setItem('user_email', data.profile.email);
              localStorage.setItem("profile", angular.toJson(data.profile));
              $rootScope.profile = data.profile;
              openDriverContext(data);

            })
            .error(function (error) {
              messageService.showError("E-mail ou senha incorretos.");
            })
            .finally(function () {
              $ionicLoading.hide();
            });
        }
      };

      loginPopup.then(function (res) {

        if (angular.isDefined(window.cordova)) {
          var elem = angular.element(document.querySelector('#emailId'));
          var isAndroid = ionic.Platform.isAndroid();
          if(isAndroid) {
              cordova.plugins.Focus.focus(elem[0]);
          }else {
              elem[0].focus();
          }
        }

        if (res === 0) {
          performLogin();
        } else if (res === 1) {
          $scope.forgotMyPassword();
        }
      });
    }

    $scope.loginWithFacebook = function () {

      localStorage.setItem('user_pass', '');
      $scope.user.password = '';

      facebookConnectPlugin.getLoginStatus(
        function (success) {
          if (success.status === "connected") {
            facebookConnectPlugin.api(success.authResponse.userID + "/?fields=id,email,name", ["email"],
              function onSuccess(result) {
                localStorage.setItem('user_email', result.email);
                localStorage.setItem('user_name', result.name);
                $state.go('signup');
              }, function onError(error) {
                console.error("Failed: ", error);
              }
            );
          } else {
            facebookConnectPlugin.login(["public_profile"],
              function (result) {
                console.log(result);
              }, function (error) {
                console.log(error);
              });
          }
        },
        function (error) {
          console.log(error);
        });
    };

    $scope.forgotMyPassword = function () {

      var myPopup = $ionicPopup.show({
        title: 'Esqueci minha Senha',
        cssClass: 'popup-vertical-buttons',
        template: '<div>' +
        '<ion-list>' +
        '<div class="item>' +
        '<h5 id="header" style="color:#747474;text-align:center;">Por favor, informe seu e-mail.  Em breve você receberá instruções para a definição de  uma nova senha.</h5>' +
        '<label class="item item-input item-floating-label" ng-class="{\'label-border-high\': hasEmailFocus}">' +
        '<input type="email" ng-model="user.email" name="email" placeholder="{{\'EMAIL\' | translate}}"' +
        'ng-required="true" ng-focus="hasEmailFocus = true" ng-blur="hasEmailFocus = false">' +
        '</label>			' +
        '</div>' +
        '</ion-list>' +
        '</div>',
        scope: $scope,
        buttons: [
          {
            text: '<b>Enviar</b>',
            type: 'button button-positive button-block',
            onTap: function () {

              var user = {user: {email: $scope.user.email}};

              $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});
              $http.post($rootScope.urlParkapp + 'users/password.json?', user)
                .success(function (data) {
                  messageService.showSuccess("As instruções de recuperação foram enviadas para o seu e-mail");
                })
                .error(function (result) {
                  messageService.showErrors(result);
                })
                .finally(function () {
                  $ionicLoading.hide();
                  return;
                });
            }
          },
          {
            text: '<b>Cancelar</b>',
            type: 'button button-stable button-block',
            onTap: function () {
              return;
            }
          }
        ]
      });

      myPopup.then(function (res) {
      });
    };

  })

  .controller('logoutCtrl', function ($scope, $http, $rootScope, $state, $ionicHistory) {

    var setLogout = function () {
      localStorage.removeItem('user_token');
      localStorage.removeItem('user_email');
      localStorage.removeItem('profile');
      localStorage.removeItem('price_tab');
      localStorage.removeItem('checkInList');

      $rootScope.isAuthenticated = false;
      $ionicHistory.clearHistory();
      $state.go('login');
    };

    $rootScope.$on('logoutCtrl:setLogout', function () {
      setLogout();
    });

    $scope.logout = function () {
      setLogout();
    }

  });

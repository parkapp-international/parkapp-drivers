angular.module('app.routes', [])

  .config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    $stateProvider

      .state('login', {
        url: '/login',
        cache: false,
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl'
      })

      .state('drivers', {
        url: '/driverDemonstration',
        cache: false,
        templateUrl: 'templates/driver.html'
      })

      .state('parkingLot', {
        url: '/parkinglotDemonstration',
        cache: false,
        templateUrl: 'templates/parkinglot.html'
      })

      .state('signup', {
        url: '/signup',
        cache: false,
        templateUrl: 'templates/signup.html',
        controller: 'signupCtrl'
      })

      .state('profile', {
        url: '/profile',
        cache: false,
        templateUrl: 'templates/profile/profile.html',
        controller: 'profileCtrl'
      })

      .state('support', {
        url: '/support',
        cache: false,
        templateUrl: 'templates/support/support.html',
        controller: 'supportCtrl'
      })

      .state('changeInfo', {
        url: '/changeInfo',
        cache: false,
        templateUrl: 'templates/profile/changeInfo.html',
        controller: 'changeInfoCtrl'
      })

      .state('changePassword', {
        url: '/changePassword',
        cache: false,
        templateUrl: 'templates/profile/changePassword.html',
        controller: 'changePasswordCtrl'
      })

      .state('driverHome', {
        url: '/driver-home',
        cache: false,
        templateUrl: 'templates/home/home.html',
        controller: 'driverHomeCtrl',
      })

      .state('myAccount', {
        url: '/my-account',
        cache: false,
        templateUrl: 'templates/myAccount/myAccount.html',
        controller: 'myAccountCtrl',
      })

      .state('addCreditCard', {
        url: '/my-account-add-credit-card',
        cache: false,
        templateUrl: 'templates/myAccount/addCreditCard.html',
        controller: 'addCreditCardCtrl',
      })

      .state('myVehicles', {
        url: '/my-vehicles',
        cache: false,
        templateUrl: 'templates/myVehicles/myVehicles.html',
        controller: 'myVehiclesCtrl',
      })

      .state('addVehicle', {
        url: '/my-vehicles-add-vehicle',
        cache: false,
        templateUrl: 'templates/myVehicles/addVehicle.html',
        controller: 'addVehicleCtrl',
      })

      .state('activeSession', {
        url: '/active-session',
        cache: false,
        templateUrl: 'templates/home/activeSession.html',
        controller: 'activeSessionCtrl',
      })

      .state('ticketScanner', {
        url: '/ticket-scanner',
        cache: false,
        templateUrl: 'templates/home/ticketScanner.html',
        controller: 'ticketScannerCtrl',
      })

    $urlRouterProvider.otherwise('/login')
  });

/**
 * Created by Márcio Santos on 10/07/2016.
 */

var app = angular.module('app.yesNo', []);

app.filter('yesNo', function($filter) {
  return function(input) {
    return input ? $filter('translate')('YES') : $filter('translate')('NO');
  }
});

/**
 * Created by Márcio Santos on 08/07/2016.
 */

var app = angular.module('app.vehiclesService', []);

app.factory('vehiclesService', function ($ionicPopup, $ionicLoading, $http, $filter, messageService, constants, $rootScope, profileService) {

  var self = {

    getColors: function() {

      return [
        {id: 1, description: 'Branco'},
        {id: 2, description: 'Preto'},
        {id: 3, description: 'Prata'},
        {id: 4, description: 'Cinza'},
        {id: 5, description: 'Azul'},
        {id: 6, description: 'Verde'},
        {id: 7, description: 'Amarelo'},
        {id: 8, description: 'Vermelho'},
        {id: 0, description: 'Outra cor'}
      ];
    },

    getPlateValue: function (letters, numbers) {

      var plateLetters = (angular.isUndefined(letters) || letters.length != 3)
        ? "" : letters;

      var plateNumbers = (angular.isUndefined(numbers) || numbers.length != 4)
        ? "" : numbers;

      return plateLetters + plateNumbers;
    },

    isValidPlate: function (plate) {
      return plate.length === 7;
    },

    registerVehiclePopup: function (scope, callback) {

      console.log(scope);

      scope.colorList = self.getColors();
      scope.vehicle = {};

    //   scope.doFocus('#plateLetraId');

      var registerVehiclePopup = $ionicPopup.show({
        template: '',
        cssClass: 'popup-vertical-buttons registerVehicle-popup',
        title: $filter('translate')('INSERT_PLATE_TO_FINISH_REGISTER'),
        templateUrl: 'templates/myVehicles/newVehiclePopupTemplate.html',
        scope: scope,
        buttons: [
          {
            text: '<b>' + $filter('translate')('ADD_VEHICLE') + '</b>',
            type: 'button-positive button-block',
            onTap: function (e) {
              return 0;
            }
          },
          {
            text: '<b>' + $filter('translate')('CANCEL') + '</b>',
            type: 'button-stable button-block',
            onTap: function (e) {
              return 1;
            }
          }
        ]
      });

      registerVehiclePopup.then(function (res) {
        if (res === 0)
          self.createUserVehicle(scope, callback);
        // else
        //   $ionicPopup.hide();
      });
    },

    createUserVehicle: function (scope, callback) {

      scope.vehicle.plate = self.getPlateValue(scope.vehicle.plateLetra, scope.vehicle.plateNumero);

      $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});

      var userVehicle = {
        user_vehicle: {
          license_plate: scope.vehicle.plate,
          model: scope.vehicle.model,
          color: scope.vehicle.color
        }
      };

      $http.post($rootScope.urlParkapp + 'account/user_vehicles', userVehicle)
        .success(function (data) {

            var profile = angular.fromJson(localStorage.getItem("profile"));

            mixpanel.identify(profile.email);
            mixpanel.people.set({
                "$first_name": profile.name,
                "$email": profile.email,
                "Perfil": "Motorista",
                "Telefone" : profile.cellphone,
                "Placa": data.vehicle.license_plate,
                "Modelo": data.vehicle.model,
                "Cor": data.vehicle.color
            });

            $http({
              url: "https://www.rdstation.com.br/api/1.3/conversions",
              method: "PUT",
              headers: {
               'Content-Type': "application/json"
              },
              data : {
                "token_rdstation": "61538b3fedfa8ea4b99f99e2aac73d8e", //Obrigatório - Token Publico de autenticação
                "identificador": "Cadastro veículo",
                "email": profile.email,
                "Placa do motorista": scope.vehicle.plate,
                "Modelo do carro": scope.vehicle.model,
                "Cor do carro": scope.vehicle.color
              }
            })
            .success(function (data) {
                console.log('bão', data);
            //   messageService.showSuccess('Mensalista cadastrado com sucesso!');
            })
            .error(function (result) {
                console.log('zicou', result);
            //   messageService.showError('Não foi possível efetuar o cadastro.');
            });
          messageService.showSuccess($filter('translate')('SUCCESS_CREATE_VEHICLE'));
          callback();
        })
        .error(function (result) {
          messageService.showErrors(result.errors);
        })
        .finally(function () {
          $ionicLoading.hide();
        });
    }
  };

  return self;

});

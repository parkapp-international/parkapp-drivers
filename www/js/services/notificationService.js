var app = angular.module('app.notificationService', [])

app.service('notificationService', function ($rootScope, $timeout, $ionicPopup, $ionicModal) {

	var self = {

		show: function(notificationData) {
			$rootScope.notificationData = notificationData;
			if(!$scope.modal) {
				$ionicModal.fromTemplateUrl('templates/connectionNotification.html', {
					scope: $rootScope,
					animation: 'animated bounceIn',
					hideDelay: 0,
					backdropClickToClose: true
				}).then(function(modal) {

					if(!$rootScope.modal) {
						$rootScope.modal = modal;
						// console.log('mostrou');
						$rootScope.modal.show();
					}
					$rootScope.hideModal = function(){
						// console.log('escondeu');
						$rootScope.modal.hide();
						$rootScope.modal.remove();
						$rootScope.modal = null;
					}
					$rootScope.showOfflineMessage = function() {
						var alertPopup = $ionicPopup.alert({
							title: 'Atenção! Você está offline!',
							template: 'Os dados só serão salvos quando você se conectar à internet.'
						});
					}
					$timeout(function () {
						if($rootScope.modal) {
							$rootScope.hideModal();
						}
					}, 5000);
				});
			}
		},

		showError: function(message) {
			self.show({
				style: 'notification-error',
				message: message
			});
		},

		showSuccess: function(message) {
			self.show({
				style: 'notification-success',
				message: message
			});
		}
	};
  	return self;
});

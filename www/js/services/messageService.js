var app = angular.module('app.messageService', [])

app.service('messageService', function ($ionicPopup, $state, $filter) {

  var self = {
    showError: function (message) {

      var alertPopup = $ionicPopup.alert({
        title: 'Erro',
        template: message
      });
      alertPopup.then(function (res) {
      });
    },

    showAlert: function (message, title) {

      var alertPopup = $ionicPopup.alert({
        title: title ? title : 'Atenção',
        template: message
      });
      alertPopup.then(function (res) {
      });
    },

    showConfirmAndDo: function(title, message, callback, param) {

      var confirmPopup = $ionicPopup.confirm({
        title: title,
        template: message
      });

      confirmPopup.then(function (res) {
        if (res) {
          callback(param);
        }
      });
    },

    showConfirmAndRedirect: function (title, message, route) {

      var alertPopup =  $ionicPopup.show({
        cssClass: 'popup-vertical-buttons',
        title: title,
        template: message,
        buttons: [
          {
            text: '<b>' + $filter('translate')('CONFIRM') + '</b>',
            type: 'button-positive button-block',
            onTap: function (e) {
              return 0;
            }
          },
          {
            text: '<b>' + $filter('translate')('CANCEL') + '</b>',
            type: 'button-stable button-block',
            onTap: function (e) {
              return 1;
            }
          }
        ]
      });
      alertPopup.then(function (res) {
        if(res === 0)
          $state.go(route);
        else
          $ionicPopup.hide();
      });
    },

    showSuccess: function (message) {

      var alertPopup = $ionicPopup.alert({
        title: 'Sucesso',
        template: message
      });
      alertPopup.then(function (res) {
      });
    },

    showErrors: function(errors) {

      var message = '';

      angular.forEach(errors, function (value, key) {
        message += key + " " + value + ".</br>";
      });

      self.showError(message);
    }

  };
  return self;
});


